public class SayayinDTO {
  private String nombre;
  private Integer nivelPoder;
  private static final Integer OVER_POWERED = 9000;

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }

  public Integer getNivelPoder() {
    return nivelPoder;
  }

  public void setNivelPoder(Integer nivelPoder) {
    this.nivelPoder = nivelPoder;
  }

  /*public String generarMensajePoder() {
    if (null != nivelPoder && OVER_POWERED.compareTo(nivelPoder) > 0) {
      return "It's over 9000";
    } else {
      return "Low power";
    }
  }*/

  public void transformar(){
	//AQUI HAY MUCHA LOGICA PROGRAMADA
}

}
